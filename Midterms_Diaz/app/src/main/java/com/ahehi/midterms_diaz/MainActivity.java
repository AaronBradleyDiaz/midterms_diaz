package com.ahehi.midterms_diaz;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


    }

    public void EatCookie(View view) {
        ImageView EatCookie = (ImageView) findViewById(R.id.before_cookie);
        EatCookie.setImageResource(R.drawable.after_cookie);
        TextView hungry = (TextView) findViewById(R.id.hungry);
        hungry.setText("I'm so full");

    }

    public void ResetCookie(View view) {
        ImageView ResetCookie = (ImageView) findViewById(R.id.before_cookie);
        ResetCookie.setImageResource(R.drawable.before_cookie);
        TextView hungry = (TextView) findViewById(R.id.hungry);
        hungry.setText("I'm so hungry");
    }
}

